defmodule SeneschalWeb.PageController do
  use SeneschalWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end

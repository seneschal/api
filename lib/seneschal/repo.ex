defmodule Seneschal.Repo do
  use Ecto.Repo,
    otp_app: :seneschal,
    adapter: Ecto.Adapters.Postgres
end

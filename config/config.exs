# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :seneschal,
  ecto_repos: [Seneschal.Repo]

# Configures the endpoint
config :seneschal, SeneschalWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "vkCC1DQ0VPgvC+1JBPKb+qgMGvD6RViB65w16h+YZe5R6I4afEzuDNP1qapSGZPj",
  render_errors: [view: SeneschalWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Seneschal.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
